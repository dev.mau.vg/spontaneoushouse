// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpontaneousGameMode.generated.h"

/**
 * 
 */
UCLASS()
class THESPONTANEOUSHOUSE_API ASpontaneousGameMode : public AGameModeBase
{
	GENERATED_BODY()

	ASpontaneousGameMode();
};
