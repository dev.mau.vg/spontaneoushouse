// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/AudioComponent.h"
#include "Blueprint/UserWidget.h"
#include "Berserker.h"
#include "Components/WidgetComponent.h"
#include "OpenDoor.h"

#include "Common_Door.generated.h"

UCLASS()
class THESPONTANEOUSHOUSE_API ACommon_Door : public AActor
{
	GENERATED_BODY()

	bool bCanBeOpened = false;
	bool bUserInputActivated = false;
	
	float CurrentYaw;
	float OriginalYaw;
	float TargetYaw = 90.0f;
	float OpenSpeed = 1.0f;

	ABerserker* PlayerInteracting = nullptr;

	USoundBase* OpenDoorSound = nullptr;
	USoundBase* CloseDoorSound = nullptr;

public:	
	ACommon_Door();
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OpenDoor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void CloseDoor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
	void ManageOpenDoor();
	
	UPROPERTY(EditAnyWhere, Category = CollisionComp)
	UBoxComponent* Box_Trigger_Component = nullptr;
	
	UPROPERTY(EditAnyWhere, Category = "Sounds")
	UAudioComponent* DoorSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Assets")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(EditAnywhere, Category="Spontaneous Behavior")
	int32 OpeningCost = 20;

	UPROPERTY(EditAnywhere, Category="Spontaneous Behavior")
	bool WasOpened = false;

	UPROPERTY(EditAnywhere, Category="Spontaneous Behavior")
	TSubclassOf<UOpenDoor> DoorSignWidget;

	UPROPERTY()
	UOpenDoor* OpenSign = nullptr;

	UFUNCTION(BlueprintCallable)
	void DisplayOpenSign();

	UFUNCTION(BlueprintCallable)
	void HideOpenSign();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
