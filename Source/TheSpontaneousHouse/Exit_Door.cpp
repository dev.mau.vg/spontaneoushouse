#include "Exit_Door.h"
#include "SpontaneousGameMode.h"

AExit_Door::AExit_Door()
{
	PrimaryActorTick.bCanEverTick = true;


	Box_Trigger_Component = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	RootComponent = Box_Trigger_Component;
	Box_Trigger_Component->SetBoxExtent(FVector(100.0f, 50.0f, 100.0f));
	Box_Trigger_Component->SetCollisionProfileName(TEXT("OverlapAll"));
	Box_Trigger_Component->SetGenerateOverlapEvents(true);
	Box_Trigger_Component->bMultiBodyOverlap = true;
	
	//Rep Visual
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	MeshComp->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> NewMesh(TEXT("StaticMesh'/Game/Geometry/Exit_Door/Door.Door'"));
	if (NewMesh.Succeeded())
	{
		MeshComp->SetStaticMesh(NewMesh.Object);
		MeshComp->SetRelativeLocation(FVector(-35.0f, -200.0f, 50.0f));
		MeshComp->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));
		MeshComp->SetWorldScale3D(FVector(4.0f));
		
		static ConstructorHelpers::FObjectFinder<UMaterial> NewMaterial(TEXT("Material'/Game/Geometry/Exit_Door/Textures/Exit_Door_M.Exit_Door_M'"));
		if(NewMaterial.Succeeded())
		{
			MeshComp->SetMaterial(0, NewMaterial.Object);
		}
	}
	
	Box_Trigger_Component->OnComponentBeginOverlap.AddDynamic(this, &AExit_Door::OpenDoor);
	Box_Trigger_Component->OnComponentEndOverlap.AddDynamic(this, &AExit_Door::CloseDoor);
	
	static ConstructorHelpers::FClassFinder<UOpenDoor> NewDoorSignWidget(TEXT("/Game/Widgets/OpenDoor_WBP"));
	if (NewDoorSignWidget.Succeeded() && NewDoorSignWidget.Class != nullptr)
	{
		DoorSignWidget = NewDoorSignWidget.Class;
	}
}

void AExit_Door::ManageOpenDoor()
{
	if (bUserInputActivated)
	{
		if (PlayerInteracting != nullptr && PlayerInteracting->GetHasKey())
		{
			PlayerInteracting->PC->WIN();
			PlayerInteracting->PC->UnPossess();
			HideOpenSign();
		}
	}
}

void AExit_Door::OpenDoor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	bUserInputActivated = true;
	EnableInput(GetWorld()->GetFirstPlayerController());
	PlayerInteracting = Cast<ABerserker>(OtherActor);
	DisplayOpenSign();
}

void AExit_Door::CloseDoor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	bUserInputActivated = false;
	PlayerInteracting = nullptr;
	DisableInput(GetWorld()->GetFirstPlayerController());
	HideOpenSign();
}

void AExit_Door::DisplayOpenSign()
{
	if(OpenSign != nullptr)
	{
		OpenSign->AddToViewport();
		OpenSign->DisplayText->SetText(FText::FromString("Exit Door"));
		if (PlayerInteracting != nullptr)
		{
			OpenSign->OpenCost->SetText(FText::FromString(
				PlayerInteracting->GetHasKey() ? "Press E to leave" : "You need the key")
			);
		}
	}
}

void AExit_Door::HideOpenSign()
{
	if(OpenSign != nullptr)
	{
		OpenSign->RemoveFromViewport();
	}
}

void AExit_Door::BeginPlay()
{
	Super::BeginPlay();
	OpenSign = CreateWidget<UOpenDoor>(GetWorld(), DoorSignWidget);
}

void AExit_Door::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

