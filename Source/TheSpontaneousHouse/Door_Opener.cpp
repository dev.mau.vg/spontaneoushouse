#include "Door_Opener.h"

#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Components/PrimitiveComponent.h"
#include "Components/AudioComponent.h"


UDoor_Opener::UDoor_Opener()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UDoor_Opener::BeginPlay()
{
	Super::BeginPlay();

	CurrentYaw = GetOwner()->GetActorRotation().Yaw;
	OriginalYaw = CurrentYaw;

	TargetYaw += CurrentYaw;

	if (Overlap_Box == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("No pressure plate assigned to %s"), *GetOwner()->GetName());
	}

	FindComponents();	
}

void UDoor_Opener::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (TotalMassOfActors() >= TotalMass)
	{
		OpenDoor(DeltaTime);
		DoorLastOpened = GetWorld()->GetTimeSeconds();
	}
	else if (GetWorld()->GetTimeSeconds() - DoorLastOpened > DoorCloseDelay)
	{
		CloseDoor(DeltaTime);
	}
}

void UDoor_Opener::OpenDoor(float DeltaTime)
{
	FRotator DoorRotation = GetOwner()->GetActorRotation();
	CurrentYaw = FMath::FInterpTo(CurrentYaw, TargetYaw, DeltaTime, OpenSpeed);
	DoorRotation.Yaw = CurrentYaw;
	GetOwner()->SetActorRotation(DoorRotation);

	if (AudioComponent != nullptr && bOpenDoor)
	{
		AudioComponent->Play();
		bOpenDoor = false;
	}
}

void UDoor_Opener::CloseDoor(float DeltaTime)
{
	FRotator DoorRotation = GetOwner()->GetActorRotation();
	CurrentYaw = FMath::FInterpTo(CurrentYaw, OriginalYaw, DeltaTime, CloseSpeed);
	DoorRotation.Yaw = CurrentYaw;
	GetOwner()->SetActorRotation(DoorRotation);

	if (AudioComponent != nullptr && !bOpenDoor)
	{
		AudioComponent->Play();
		bOpenDoor = true;
	}
}

void UDoor_Opener::FindComponents()
{
	AudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();

	if (AudioComponent == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("No Audio Component assigned to %s"), *GetOwner()->GetName());
	}	
}

float UDoor_Opener::TotalMassOfActors() const
{
	float MassSum = 0.0f;

	if (Overlap_Box != nullptr)
	{
		TArray<AActor*> OverlappingActors;
		Overlap_Box->GetOverlappingActors(OverlappingActors);

		for (AActor* x : OverlappingActors)
		{
			MassSum += x->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		}
	}

	return MassSum;
}