// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Levitate.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THESPONTANEOUSHOUSE_API ULevitate : public UActorComponent
{
	GENERATED_BODY()

float time = 0.1f;
	
public:	
	// Sets default values for this component's properties
	ULevitate();

	UPROPERTY(EditAnywhere)
	float Speed = 20.0f;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
