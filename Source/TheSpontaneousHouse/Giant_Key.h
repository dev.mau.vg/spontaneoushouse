// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "KeySpawner.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"

#include "Giant_Key.generated.h"

UCLASS()
class THESPONTANEOUSHOUSE_API AGiant_Key : public AActor
{
	GENERATED_BODY()

	UParticleSystem* EmitterTemplate = nullptr;

	
public:	
	// Sets default values for this actor's properties
	AGiant_Key();
	void SelectSpawner();
	void ExtendLocations();

	UPROPERTY(EditAnyWhere, Category = CollisionComp)
	UBoxComponent* Box_Trigger_Component = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Assets")
	UStaticMeshComponent* MeshComp;
	
	UPROPERTY(EditAnyWhere, Category = Spawners)
	TArray<AKeySpawner*> SpawnLocations;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void PickUp(class UPrimitiveComponent* OverlappedComp,
		class AActor* OtherActor,
		class UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
};
