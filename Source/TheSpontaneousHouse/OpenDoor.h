// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"

#include "OpenDoor.generated.h"

/**
 * 
 */
UCLASS()
class THESPONTANEOUSHOUSE_API UOpenDoor : public UUserWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* DisplayText = nullptr;
	
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* OpenCost = nullptr;
	
};
