// Fill out your copyright notice in the Description page of Project Settings.


#include "Giant_Key.h"

#include "Berserker.h"
#include "KeySpawner.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"

// Sets default values

// Called when the game starts or when spawned
AGiant_Key::AGiant_Key()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	//Rep Fisica
	Box_Trigger_Component = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	RootComponent = Box_Trigger_Component;
	Box_Trigger_Component->SetBoxExtent(FVector(70.0f, 20.0f, 20.0f));
	Box_Trigger_Component->SetCollisionProfileName(TEXT("OverlapAll"));
	Box_Trigger_Component->SetGenerateOverlapEvents(true);
	Box_Trigger_Component->bMultiBodyOverlap = true;
	
	//Rep Visual
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	MeshComp->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> NewMesh(TEXT("StaticMesh'/Game/Geometry/Exit_Key.Exit_Key'"));
	if (NewMesh.Succeeded())
	{
		MeshComp->SetStaticMesh(NewMesh.Object);
		MeshComp->SetRelativeLocation(FVector(-20.0f, 0.0f, 0.0f));
		MeshComp->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
		MeshComp->SetWorldScale3D(FVector(1.0f));
		MeshComp->SetCollisionProfileName(TEXT("OverlapAll"));
	}

	Box_Trigger_Component->OnComponentBeginOverlap.AddDynamic(this, &AGiant_Key::PickUp);

	static ConstructorHelpers::FObjectFinder<UParticleSystem> NewParticle(TEXT("ParticleSystem'/Game/TS_Fireworks/Particles/PS_TS_Fireworks_Burst_ShellsWithinShells.PS_TS_Fireworks_Burst_ShellsWithinShells'"));
	if (NewParticle.Succeeded())
	{
		EmitterTemplate = NewParticle.Object;
	}
}

void AGiant_Key::SelectSpawner()
{
	unsigned int RandLocation = FMath::RandRange(0, SpawnLocations.Num()-1);
	SetActorLocation(SpawnLocations[RandLocation]->MeshComp->GetComponentLocation());
	
	SpawnLocations.Empty();
}

void AGiant_Key::ExtendLocations()
{
	int OriginalElements = SpawnLocations.Num(); 
	
	for (int i = 0; i < OriginalElements; ++i)
	{
		for (unsigned int j = 0; j < SpawnLocations[i]->ChancesOfSpawning; ++j)
		{
			AKeySpawner* NewKeySpawner = SpawnLocations[i];
			SpawnLocations.Add (NewKeySpawner);
		}
	}
}

void AGiant_Key::BeginPlay()
{
	Super::BeginPlay();

	if (SpawnLocations.Num() > 0)
	{
		ExtendLocations();
		SelectSpawner();
	}
}

// Called every frame
void AGiant_Key::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGiant_Key::PickUp(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ABerserker* PlayerInteracting = Cast<ABerserker>(OtherActor);
	if (PlayerInteracting != nullptr)
	{
		PlayerInteracting->SetHasKey();
	}

	UGameplayStatics::SpawnEmitterAtLocation(
		GetWorld(),
		EmitterTemplate,
		GetActorLocation(),
		FRotator(0.0f, 0.0f, 0.0f),
		true);
	
	this->Destroy();
}

