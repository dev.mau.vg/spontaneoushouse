// Fill out your copyright notice in the Description page of Project Settings.


#include "Levitate.h"

// Sets default values for this component's properties
ULevitate::ULevitate()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void ULevitate::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void ULevitate::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FRotator KeyRotation = GetOwner()->GetActorRotation();
	FVector pos = GetOwner()->GetActorLocation();
	
	KeyRotation.Yaw += (Speed * DeltaTime);
	GetOwner()->SetActorRotation(KeyRotation);

	time += DeltaTime / 2;
	const float DeltaHeight = (FMath::Sin((time + DeltaTime)) - FMath::Sin((time)));
	pos.Z += DeltaHeight * Speed;
	GetOwner()->SetActorLocation(pos);
}

