// Fill out your copyright notice in the Description page of Project Settings.


#include "KeySpawner.h"

#include "Algo/RandomShuffle.h"
#include "Components/BoxComponent.h"

// Sets default values
AKeySpawner::AKeySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Rep Fisica
	Box_Trigger_Component = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	RootComponent = Box_Trigger_Component;
	Box_Trigger_Component->SetBoxExtent(FVector(70.0f, 20.0f, 20.0f));
	Box_Trigger_Component->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	//Rep Visual
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	MeshComp->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> NewMesh(TEXT("StaticMesh'/Game/Geometry/Exit_Key.Exit_Key'"));
	if (NewMesh.Succeeded())
	{
		MeshComp->SetStaticMesh(NewMesh.Object);
		MeshComp->SetRelativeLocation(FVector(-20.0f, 0.0f, 0.0f));
		MeshComp->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
		MeshComp->SetWorldScale3D(FVector(1.0f));
		MeshComp->SetCollisionProfileName(TEXT("OverlapAll"));
	}

	MeshComp->SetHiddenInGame(true);

}

// Called when the game starts or when spawned
void AKeySpawner::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AKeySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

