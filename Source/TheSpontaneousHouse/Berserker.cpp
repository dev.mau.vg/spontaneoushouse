// Fill out your copyright notice in the Description page of Project Settings.

#include "Berserker.h"
#include "DrawDebugHelpers.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ABerserker::ABerserker()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(42.0f, 75.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	UCharacterMovementComponent* MoveComp = GetCharacterMovement();
	if (MoveComp != nullptr)
	{
		MoveComp->bOrientRotationToMovement = true;
		MoveComp->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
		MoveComp->JumpZVelocity = 600.0f;
		MoveComp->AirControl = 0.2f;
	}

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 400.f;
	SpringArm->SetRelativeLocation(FVector(0.0f,0.0f, 90.0f));

	// Rotating Controller
	SpringArm->bUsePawnControlRotation = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false;
	Camera->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));

	Skeleton = GetMesh();
	Skeleton->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> NewMesh(TEXT("SkeletalMesh'/Game/Mannequin/Character/Mesh/SK_Mannequin.SK_Mannequin'"));
	static ConstructorHelpers::FObjectFinder<UAnimBlueprint> NewAnim(TEXT("AnimBlueprint'/Game/Mannequin/Animations/ThirdPerson_AnimBP.ThirdPerson_AnimBP'"));
	if (NewMesh.Succeeded())
	{
		Skeleton->SetSkeletalMesh(NewMesh.Object);
		Skeleton->SetRelativeLocation(FVector(0.0f, 0.0f, -75.0f));
		Skeleton->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));
		Skeleton->SetWorldScale3D(FVector(1.0f));
		if (NewAnim.Succeeded())
		{
			Skeleton->SetAnimClass(NewAnim.Object->GeneratedClass);
		}
	}
}

// Called when the game starts or when spawned
void ABerserker::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABerserker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CheckTrace(DeltaTime);
}

// Called to bind functionality to input
void ABerserker::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	PC = Cast<ABerserkerPlayerController>(GetController());
}

void ABerserker::MoveForward(float AxisValue)
{
	if ((PC != nullptr) && (AxisValue > 0 || AxisValue < 0))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.0f, Rotation.Yaw, 0.0f);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, AxisValue);
	}
}

void ABerserker::MoveHorizontal(float AxisValue)
{
	if ((PC != nullptr) && (AxisValue > 0 || AxisValue < 0))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.0f, Rotation.Yaw, 0.0f);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, AxisValue);
	}
}

void ABerserker::Turn(float AxisValue)
{
	if ((PC != nullptr) && (AxisValue > 0 || AxisValue < 0))
	{
		PC->AddYawInput(AxisValue);
	}
}

void ABerserker::TurnY(float AxisValue)
{
	if ((PC != nullptr) && (AxisValue > 0 || AxisValue < 0))
	{
		PC->AddPitchInput(-AxisValue);
	}
}

void ABerserker::SpendHumanity(float HumanityAmount)
{
	if (PC != nullptr && HumanityAmount > 0)
	{
		PC->SpendHumanity(HumanityAmount);
	}
}

bool ABerserker::GetHasKey() const
{
	if (PC != nullptr)
	{
		return PC->GetHasKey();
	}
	return false;
}

void ABerserker::SetHasKey()
{
	if (PC != nullptr)
	{
		PC->SetHasTheKey();
	}
}

void ABerserker::CheckTrace(float DeltaTime)
{
	HumanityBooster = nullptr;
	FVector Start = GetActorLocation();
	FVector End = (GetActorForwardVector() * 100.0f) + Start;

	FCollisionQueryParams CollisionParams;

	float dotProduct = FVector::DotProduct(GetActorForwardVector(), GetActorForwardVector());

	FHitResult Hit;

	if (GetWorld()->LineTraceSingleByChannel(Hit,Start,End,ECC_Visibility,CollisionParams))
	{
		if (Hit.Actor != nullptr)
		{
			HumanityBooster = Cast<AHumanityBooster>(Hit.Actor);
			if (HumanityBooster != nullptr)
			{
				float HumanityRestoration = HumanityBooster->UseBooster(PC->GetHumanityLevel() ,DeltaTime);
				PC->SpontaneousDamage(HumanityRestoration);
			}
		}
	}
}