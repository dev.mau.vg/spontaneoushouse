// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HumanityBooster.generated.h"

UCLASS()
class THESPONTANEOUSHOUSE_API AHumanityBooster : public AActor
{
	GENERATED_BODY()
	
	bool bVisited = false;
	float AmountAvailable = 0.0f;

public:	
	// Sets default values for this actor's properties
	AHumanityBooster();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Assets")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Assets")
	float Multiplier = 10.0f;

	float UseBooster(float HumanityAmount, float DeltaTime);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
